pub enum ColorScheme
{
	Light,
	Dark,
	NoPreference
}

impl ToString for ColorScheme
{
	fn to_string(&self) -> String
	{
		return match self {
			ColorScheme::Light => "Light mode".to_owned(),
			ColorScheme::Dark => "Dark mode".to_owned(),
			ColorScheme::NoPreference => "No preference".to_owned()
		};
	}
}

impl Into<u8> for ColorScheme
{
	fn into(self) -> u8
	{
		return self.to_u8();
	}
}

impl From<dark_light::Mode> for ColorScheme
{
	fn from(value: dark_light::Mode) -> Self
	{
		return match value {
			dark_light::Mode::Dark => Self::Dark,
			dark_light::Mode::Light => Self::Light,
			dark_light::Mode::Default => Self::NoPreference
		};
	}
}

impl ColorScheme
{
	pub fn to_u8(&self) -> u8
	{
		// We'll use the xdg-desktop-portal values
		return match self {
			ColorScheme::Light => 2,
			ColorScheme::Dark => 1,
			ColorScheme::NoPreference => 0
		};
	}
}
