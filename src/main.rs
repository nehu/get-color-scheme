mod color_scheme;

use clap::{Arg, ArgMatches, Command};
use color_scheme::ColorScheme;

fn main()
{
	let cli_matches = init_cli();

	let scheme = ColorScheme::from(dark_light::detect());

	if cli_matches.contains_id("verbose") {
		println!("Current color scheme: {}", scheme.to_string());
	}
	else {
		println!("{}", scheme.to_u8());
	}
}

fn init_cli() -> ArgMatches
{
	// clap v4 introduces a lot of breaking changes,
	// no longer offers a simple and nice colored output,
	// and has a different - less optimal - help template.

	//let style = styling::Styles::styled()
	//	.header(styling::AnsiColor::Yellow.on_default() | styling::Effects::BOLD)
	//	.usage(styling::AnsiColor::Yellow.on_default() | styling::Effects::BOLD)
	//	.literal(styling::AnsiColor::Green.on_default())
	//	.placeholder(styling::AnsiColor::Cyan.on_default());

	return Command::new(env!("CARGO_CRATE_NAME"))
		.version(env!("CARGO_PKG_VERSION"))
		.about(
			"Retrieves the current color scheme, i.e. 2 (\"light mode\"), 1 (\"dark mode\") or 0 (\"no preference\")."
		)
		//.color(ColorChoice::Always)
		//.styles(style)
		//.before_help("Test test test")
		////.help_template()
		.arg(
			Arg::new("verbose")
				.short('v')
				.takes_value(false) //.num_args(0)
				.help("Display a human-readable description instead of a numeric key")
		)
		.get_matches();
}
