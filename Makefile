build:
	cargo build --release

# Formatting requires a nightly toolchain with the rustfmt component, which can be installed as follows:
# - rustup toolchain install nightly --allow-downgrade --profile minimal --component rustfmt
format:
	cargo +nightly fmt

run:
	./target/release/get-appearance

# Benchmarking is done with hyperfine (https://github.com/sharkdp/hyperfine).
benchmark:
	hyperfine --warmup 3 ./target/release/get-appearance
