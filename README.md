# get-color-scheme

A CLI application to retrieve the current color scheme.

The underlying library, [dark-light](https://lib.rs/crates/dark-light), supports common operating systems and WASM.
On Linux and BSD, the XDG portal API is first queried, with a fallback to the respective desktop environment settings.

```
Retrieves the current color scheme, i.e. 2 ("light mode"), 1 ("dark mode") or 0 ("no preference").

USAGE:
    get-color-scheme [OPTIONS]

OPTIONS:
    -h, --help       Print help information
    -v               Display a human-readable description instead of a numeric key
    -V, --version    Print version information
```
